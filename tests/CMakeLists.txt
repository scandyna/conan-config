# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.15)

project(MdtConanConfig_Test LANGUAGES CXX)

find_package(MdtCMakeModules REQUIRED)

include(MdtAddTest)

enable_testing()

message(DEBUG "CMAKE_CXX_FLAGS: ${CMAKE_CXX_FLAGS}")

mdt_add_test(
  NAME SimpleTest_Test
  TARGET simpleTest_Test
  SOURCE_FILES
    main.cpp
)
