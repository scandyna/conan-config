from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake
#import os

class MdtConanConfigTestConan(ConanFile):
  name = "MdtConanConfig"
  license = "BSD 3-Clause"
  url = "https://gitlab.com/scandyna/conan-config"
  description = "Shared Conan configuration"
  settings = "os", "compiler", "build_type", "arch"
  options = {"shared": [True, False]}
  default_options = {"shared": True}
  generators = "CMakeDeps", "CMakeToolchain", "VirtualBuildEnv"

  #def requirements(self):
    ## See: https://gitlab.com/scandyna/conan-config/-/issues/5
    #self.requires("double-conversion/3.3.0")

  def build_requirements(self):
    #self.tool_requires("catch2/2.13.9", force_host_context=True)
    #self.tool_requires("MdtCMakeModules/0.19.0@scandyna/testing", force_host_context=True)
    self.test_requires("MdtCMakeModules/0.19.0@scandyna/testing")
