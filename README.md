# Shared Conan configuration

Before installing this configuration,
please read the
[conan config](https://docs.conan.io/en/latest/reference/commands/consumer/config.html)
documentation and understand what it does, mainly what files will be overwritten
in your home directory.

```shell
conan config install --source-folder conan https://gitlab.com/api/v4/projects/16304913/packages/generic/conan1_config/$VERSION/conan_config.zip
```

# Qt profiles

Qt profiles mainly defines options specific to Qt,
typically the list of modules to build.

To avoid explosion of profiles,
some naming are described here.

## qt_core

The qt_core profile only includes [Qt Core](https://doc.qt.io/qt-5/qtcore-index.html).

Noticle that some modules are also included in this profile, because they cannot be built separately with current conanfile.py:
 - [Qt Test](https://doc.qt.io/qt-5/qttest-index.html)
 - [Qt Concurrent](https://doc.qt.io/qt-5/qtconcurrent-index.html)
 - [Qt D-Bus](https://doc.qt.io/qt-5/qtdbus-index.html)
 - [Qt Network](https://doc.qt.io/qt-5/qtnetwork-index.html)
 - [Qt XML](https://doc.qt.io/qt-5/qtxml-index.html)


## qt_core_modules

The qt_core_modules profile includes qt_core
and some modules that depends on Qt Core .

Example of such modules:
 - [Qt SQL](https://doc.qt.io/qt-5/qtsql-index.html)
 - [Qt Serial Port](https://doc.qt.io/qt-5/qtserialport-index.html)


## qt_gui_modules

The qt_gui_modules includes what is in qt_core_modules, plus:
 - [Qt GUI](https://doc.qt.io/qt-5/qtgui-index.html)


# Rationale

## Including Linux distribution

### Situation

I'm using Ubuntu-18.04 at home, so the [docker images used in the CI](https://gitlab.com/scandyna/docker-images-ubuntu)
have been initially based on the same distribution.

Having trouble to build Qt because Python was to old (3.6, required is 3.8),
I started to build Qt on a Ubuntu-20.04 based image.

Once I wanted to use those builds on a Ubuntu-18.04, link errors occured.

By default, the Linux distribution is not encoded in the package id.
This means that core system libraries, like the glibc version, is not encoded.

As result, packages built on a Ubuntu-20.04 have been used to build on a Ubuntu-18.04,
with a older glibc (and probably other incompatible low level stuff ?).

### First attempt

I added the distribution Linux subsetting in the `settings.yml`
and updated the Linux profiles to use it.

Now I have to rebuild anything that is available on [Conan center](https://conan.io/center),
including build requirements.

Conan center build their packages on a Ubuntu-16.04 image.
At least the build requirements should not cause any issues to run on newer versions.

### Second try

Split profiles into explicit names for those modeling the Linux distribution.

### Useful links

- https://github.com/conan-io/docs/issues/1137
- https://github.com/conan-io/conan-docker-tools
